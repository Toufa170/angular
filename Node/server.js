const express = require('express');
const app     = express();
app.use(express.json());
app.use(express.urlencoded({extend:true}));
app.use(function (req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
	res.setHeader('Access-Control-Allow-Headers', '*');
	next();
});
//app.use(require("cors"));(methode alternative)

const MongoClient = require('mongodb').MongoClient;
const ObjectID    = require('mongodb').ObjectId;
const url         = "mongodb://localhost:27017";

MongoClient.connect(url, {useNewUrlParser: true}, (err, client) => {
	let db = client.db("SUPERVENTES");

	/*liste des produits*/

	app.get("/Produits", (req,res) => {
		console.log("/Produits");
		try {
			db.collection("Produits").find().toArray((err, documents) => {
				res.end(JSON.stringify(documents));
			});
		} catch(e) {
			console.log("Erreur sur /produits: " + e);
			res.end(JSON.stringify([]));
		}
	}) ;	
// recuperation de la liste par categorie
// produits root au niveau de localhost

	app.get("/Produits/:categorie", (req,res) => {
		let categorie = req.params.categorie; // mettre ds variable categorie
		console.log("/Produits/"+categorie);
		try {
			
			db.collection("Produits").find({type:categorie}).toArray((err,documents) => {
				res.end(JSON.stringify(documents));
			});
		} catch(e) {
			console.log("erreur sur /Produits/"+categorie+" : "+ e);
			res.end(JSON.stringify([]));
		} //message en cas d'erreur
	});
	app.get("/categories", (req, res) => {
		console.log("/categories");
		categories = [];
		try {
			db.collection("Produits").find().toArray((err, documents) => {
                 for(let doc of documents) {
                 	if (!categories.includes(doc.type)) 
                 		categories.push(doc.type);
                }
				console.log("Renvoi de"+JSON.stringify(categories));
				res.end(JSON.stringify(categories));
            });
	
        } catch(e) {
	        console.log("Erreur sur /categories : " + e);
	        res.end(JSON.stringify([]));
        }
    });
	
	/* Connexion*/
    app.post("/membre/connexion", (req,res) => {
	    console.log("/utilisateurs/connexion avec "+JSON.stringify(req.body));
	    try {
		    db.collection("Users")
		      .find(req.body)
		      .toArray((err, documents) => {
		  	    if (documents.lenght == 1) // document qui sera renvoyé
		  		    res.end(JSON.stringify({"resultat": 1, "message": "authentification réussie"}));
		  	    else res.end(JSON.stringify({"resultat": 0, "message": "Email et/ou mot de passe incorrect"}));
		    });

        } catch (e) {
    	    res.end(JSON.stringify({"resultat": 0, "message": e}));
        }
	});
	});
// precision du port
	
app.listen(8888); 